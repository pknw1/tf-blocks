#!/bin/bash
find . -mindepth 1 -maxdepth 1 -type d | grep -v 01 |sort| while read -r DIR
do

echo $DIR
cd $DIR
  terraform destroy -auto-approve
cd ..
echo $(pwd)
done
