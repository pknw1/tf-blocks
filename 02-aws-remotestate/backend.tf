terraform {
  backend "s3" {
    bucket = "automationlogic-paulkelleher-tfstate"
    key    = "paulkelleher-tfstate/project"
    region = "us-east-2"
    dynamodb_table = "automationlogic-paulkelleher-statelock"
    encrypt = true
    kms_key_id     = "alias/terraform-bucket-key"
    shared_credentials_file = "$HOME/.aws/credentials"

  }
}
