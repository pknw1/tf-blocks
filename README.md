
```

                _                        _   _             _                 _      
     /\        | |                      | | (_)           | |               (_)     
    /  \  _   _| |_ ___  _ __ ___   __ _| |_ _  ___  _ __ | |     ___   __ _ _  ___ 
   / /\ \| | | | __/ _ \| '_ ` _ \ / _` | __| |/ _ \| '_ \| |    / _ \ / _` | |/ __|
  / ____ \ |_| | || (_) | | | | | | (_| | |_| | (_) | | | | |___| (_) | (_| | | (__ 
 /_/    \_\__,_|\__\___/|_| |_| |_|\__,_|\__|_|\___/|_| |_|______\___/ \__, |_|\___|
                                                                        __/ |       
                                                                       |___/        
```

### Development Provider Prep
#### Automation User
#### tfstate and terraform lock backend
#### Resource access and policy
###
```
.
├── apply.sh
├── destroy.sh
├── push.sh
├── 01-aws-tfstate
│   ├── provider.tf
│   ├── s3.tf
│   └── variables.tf
├── 02-aws-remotestate
│   ├── backend.tf
│   ├── provider.tf
│   └── variables.tf
├── 03-networking
│   ├── backend.tf
│   ├── network.tf
│   ├── provider.tf
│   └── variables.tf
├── 04-ec2
│   ├── backend.tf
│   ├── provider.tf
│   ├── simple-ec2-public-ssh.tf
│   ├── simple-ec2.tf
│   └── variables.tf
├── 05-s3
│   ├── backend.tf
│   ├── provider.tf
│   ├── s3.tf
│   └── variables.tf
└── README.md

```


| Section  | Information | ToDo |
|----------|---|---|
| Scripts  | push: commit and push current branch<br> apply: terraform apply all folders <br> destroy: terraform destroy all folders  |   |  
| tfstate  | in order to remotely manage your terraform state, we need to creat the backend S3 and Dynamo DB |   | 
| s3 state | template files setting the remote backend  |  |
| networks | example VPC networking  |  |
| compute  | exqmple EC2 instance plus SSH Access |  |
| storage  | example S3 bucket  |  |

