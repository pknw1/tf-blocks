terraform {
  backend "s3" {
    bucket = "automationlogic-paulkelleher-tfstate"
    key    = "paulkelleher-tfstate/s3bucket"
    region = "us-east-2"
    dynamodb_table = "automationlogic-paulkelleher-statelock"
    encrypt = true
    shared_credentials_file = "$HOME/.aws/credentials"
    kms_key_id     = "alias/terraform-bucket-key"


  }
}
