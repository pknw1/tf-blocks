resource "aws_kms_key" "mykey" {
  description             = "This key is used to encrypt bucket objects"
  deletion_window_in_days = 10

  tags = var.default_tags

}


resource "aws_s3_bucket" "home-paulkelleher" {
  bucket = "automationlogic-paulkelleher-test"
  acl    = "private"
  versioning {
    enabled = true
  }
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = aws_kms_key.mykey.arn
        sse_algorithm     = "aws:kms"
      }
    }
  }
  
  tags = var.default_tags


}

resource "aws_s3_bucket_public_access_block" "home-paulkelleher" {
  bucket = aws_s3_bucket.home-paulkelleher.id

  block_public_acls   = true
  block_public_policy = true
  ignore_public_acls      = true
  restrict_public_buckets = true


}
