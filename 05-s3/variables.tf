variable "region" {
  default = "us-east-2"
}

variable "ami_key_pair_name" {
    default = "default"
}

variable "environment" {
    default = "sandbox"
}

variable "default_tags" { 
    type = map 
    default = { 
        project: "paulkellehersandbox",
        component: "template-s3-backend",
        info: "paul kelleher dev area",
        environment: "sandbox"
  } 
}


