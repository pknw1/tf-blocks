#!/bin/bash
find . -mindepth 1 -maxdepth 1 -type d | grep -v 01 |sort| while read -r DIR
do

cd $DIR
  terraform apply -auto-approve
cd ..
done
