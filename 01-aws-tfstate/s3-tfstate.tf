resource "aws_kms_key" "tfstate" {
  description             = "This key is used to encrypt bucket objects"
  deletion_window_in_days = 10
  enable_key_rotation     = true

  tags = var.default_tags

}

resource "aws_kms_alias" "key-alias" {
 name          = "alias/terraform-bucket-key"
 target_key_id = aws_kms_key.tfstate.key_id
}


resource "aws_s3_bucket" "home-paulkelleher" {
  bucket = "automationlogic-paulkelleher-tfstate"
  acl    = "private"
  versioning {
    enabled = true
  }
#  server_side_encryption_configuration {
#    rule {
#      apply_server_side_encryption_by_default {
#        kms_master_key_id = aws_kms_key.tfstate.arn
#        sse_algorithm     = "aws:kms"
#      }
#    }
#  }
  
  tags = var.default_tags


}

resource "aws_s3_bucket_policy" "terraform_state" {
  bucket = "${aws_s3_bucket.home-paulkelleher.id}"
  policy =<<EOF
{
  "Version": "2012-10-17",
  "Id": "RequireEncryption",
   "Statement": [
    {
      "Sid": "RequireEncryptedTransport",
      "Effect": "Deny",
      "Action": ["s3:*"],
      "Resource": ["arn:aws:s3:::${aws_s3_bucket.home-paulkelleher.bucket}/*"],
      "Condition": {
        "Bool": {
          "aws:SecureTransport": "false"
        }
      },
      "Principal": "*"
    },
    {
      "Sid": "RequireEncryptedStorage",
      "Effect": "Deny",
      "Action": ["s3:PutObject"],
      "Resource": ["arn:aws:s3:::${aws_s3_bucket.home-paulkelleher.bucket}/*"],
      "Condition": {
        "StringNotEquals": {
          "s3:x-amz-server-side-encryption": "AES256"
        }
      },
      "Principal": "*"
    }
  ]
}
EOF
}

resource "aws_s3_bucket_public_access_block" "home-paulkelleher" {
  bucket = aws_s3_bucket.home-paulkelleher.id

#  block_public_acls   = true
#  block_public_policy = true
#  ignore_public_acls      = true
#  restrict_public_buckets = true


}

resource "aws_dynamodb_table" "terraform_state_lock" {
  name           = "automationlogic-paulkelleher-statelock"
  read_capacity  = 1
  write_capacity = 1
  hash_key       = "LockID"

  attribute {
    name = "LockID"
    type = "S"
  }
}
