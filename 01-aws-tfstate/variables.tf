variable "environment" {
    default = "sandbox"
}

variable "default_tags" { 
    type = map 
    default = { 
        project: "paulkellehersandbox",
        component: "terraform state backend",
        info: "paul kelleher dev area",
        environment: "sandbox"
  } 
}


